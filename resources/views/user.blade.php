<!DOCTYPE html>
<html>
<head>
	<title>Home Page</title>
</head>
<body>
	<table>
		<thead>
			<tr>
				<th>Name</th>
				<th>Phone</th>
			</tr>
		</thead>
		<tbody>
			@foreach($datas as $data)
				<tr>
					<td>{{ $data['name'] }}</td>
					<td>{{ $data['phone'] }}</td>
				</tr>
			@endforeach
		</tbody>
	</table>
</body>
</html>