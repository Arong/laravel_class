<!DOCTYPE html>
<html>
<head>
	<title>Post Home Page</title>
</head>
<body>
	@foreach($datas as $data)
		<div>{{ $data['title'] }}</div>
	@endforeach
	<form action="{{ url("/post") }}" method="post">
		{{ csrf_field() }}
		文章標題:<input type="text" name="title">
		內容:<input type="text" name="content">
		建立者:<input type="text" name="name">
		圖片名稱:<input type="text" name="picture_name">
		圖片路徑:<input type="text" name="picture_path">
		<button type="submit">建立</button>
	</form>
</body>
</html>