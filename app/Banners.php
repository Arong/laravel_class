<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banners extends Model
{
    public $table = 'banners';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
    	'path'
    ];

    protected $casts = [
    	'id' => 'integer',
    	'path' => 'string'
    ];
}
