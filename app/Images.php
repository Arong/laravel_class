<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Images extends Model
{
    public $table = 'images';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
    	'name',
    	'path'
    ];

    protected $casts = [
    	'id' => 'integer',
    	'name' => 'string',
    	'path' => 'string'
    ];
}
