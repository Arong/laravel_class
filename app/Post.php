<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
	public $table = 'post';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
    	'title',
    	'content',
    	'name',
    	'picture_path',
    	'picture_name',
    ];

    protected $casts = [
    	'id' => 'integer',
    	'title' => 'string',
    	'content' => 'string',
    	'name' => 'string',
        'picture_path' => 'string',
    	'picture_name' => 'string'
    ];
}
