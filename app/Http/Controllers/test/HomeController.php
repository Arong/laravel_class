<?php

namespace App\Http\Controllers\test;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{
	private $number;
	protected $user;

	/**
	 * 使用者首頁
	 *
	 * @param  integer $id    使用者ID
	 * @param  [type] $status [description]
	 *
	 * @return [type]         [description]
	 */
    public function index()
    {
    	return response()->json([
    		'code' => 200,
    		'data' => 'Arong'
    	]);
    }

    public function test(Request $request)
    {
    	return view('welcome');
    }

    public function today()
    {
    	$date = date('Y-m-d H:i:s');
    	return view('today', ['today' => $date]);
    }

    public function MT()
    {
    	$dataArr = [];
    	$number = 3;

    	for ($i=1;$i<10;$i++) {
			$result = $number * $i;
			$dataArr[] = $number . 'x' . $i . '=' . $result;
		}

		// 時間
		$time = date('Y-m-d H:i:s');

		return view('multiple')
			->with('datas', $dataArr)
			->with('time', $time);
    }

    public function list()
    {
    	$datas = [
    		[
    			'name' => 'Arong',
    			'phone' => '123'
    		],
    		[
    			'name' => 'Nick',
    			'phone' => '456'
    		],
    		[
    			'name' => 'John',
    			'phone' => '789'
    		]
    	];

    	return view('user', compact('datas'));
    }

    public function listAPI()
    {
    	$datas = [
    		[
    			'name' => 'Arong',
    			'phone' => '123'
    		],
    		[
    			'name' => 'Nick',
    			'phone' => '456'
    		],
    		[
    			'name' => 'John',
    			'phone' => '789'
    		]
    	];

    	return response()->json([
    		'code' => 200,
    		'data' => $datas,
    		'messages' => 'success'
    	]);
    }
}
