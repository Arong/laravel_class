<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class PostController extends Controller
{
	public function createPost()
	{
		$datas = [];
		return view('createPost', compact('datas'));
	}

    public function store(Request $request)
    {
    	Post::create($request->all());
    	return redirect('/index');
    }

    public function update($id, Request $request)
    {
    	Post::where('id', $id)->update($request->all());
    	return redirect('/index');
    }

    public function destory($id, Request $request)
    {
    	Post::where('id', $id)->delete();
    	return redirect('/index');
    }

    public function show($id)
    {
    	$datas = Post::get();
    	return response()->json([
    		'code' => 200,
    		'data' => $datas
    	]);
    	// return view('createPost', compact('datas'));
    }

    public function list()
    {
    	$datas = Post::get();

    	return view('createPost', compact('datas'));
    }
}
