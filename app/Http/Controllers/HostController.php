<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Banners;
use App\News;
use App\Forms;
use App\Images;

class HostController extends Controller
{
    public function index()
    {
    	$datas = [
    		'banners' => Banners::get(),
    		'news' => News::get(),
    		'images' => Images::get()
    	];
    	return view('index', compact('datas'));
    }

    public function newsStore(Request $request)
    {
    	# code...
    }

    public function formsStore(Request $request)
    {
    	# code...
    }

    public function update($id, Request $request)
    {
    	# code...
    }

    public function destory($id, Request $request)
    {
    	# code...
    }

    public function show($id)
    {
    	$datas = Images::where('id', $id)->first();
    	return response()->json([
    		'code' => 200,
    		'data' => $datas
    	]);
    }

    public function list()
    {
    	$datas = Images::get();
    	return response()->json([
    		'code' => 200,
    		'data' => $datas
    	]);
    }
}
