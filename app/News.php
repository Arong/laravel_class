<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    public $table = 'news';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
    	'path',
    	'title',
    	'content'
    ];

    protected $casts = [
    	'id' => 'integer',
    	'path' => 'string',
    	'title' => 'string',
    	'content' => 'string'
    ];
}
