<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Forms extends Model
{
    public $table = 'forms';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
    	'name',
    	'email',
    	'message'
    ];

    protected $casts = [
    	'id' => 'integer',
    	'name' => 'string',
    	'email' => 'string',
    	'message' => 'string'
    ];
}
