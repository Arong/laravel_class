<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/index', function () {
	return view('index');
});

Route::get('/home', function () {
	return view('users.home');
});

Route::get('/run_controller', 'test\HomeController@index');

Route::get('/test', 'test\HomeController@test');

Route::get('/today', 'test\HomeController@today');

Route::get('/three_mt', 'test\HomeController@MT');

Route::get('/list', 'test\HomeController@list');

// Post == Put
// Put != Post
Route::group(['prefix' => 'post'], function () {
	Route::get('/', 'PostController@createPost');
	Route::post('', 'PostController@store');
	Route::put('/{id}', 'PostController@update');
	Route::delete('/{id}', 'PostController@destroy');
	Route::get('/{id}', 'PostController@show')->where('id', '[0-9]+');
	Route::get('/list', 'PostController@list');
});

Route::group(['prefix' => 'news'], function () {
	Route::post('/', 'HostController@newsStore');
});

Route::group(['prefix' => 'banners'], function () {
	Route::post('/', 'HostController@bannersStore');
});

Route::group(['prefix' => 'forms'], function () {
	Route::post('/', 'HostController@formsStore');
});

Route::group(['prefix' => 'images'], function () {
	Route::get('/', 'HostController@index');
	Route::get('/list', 'HostController@list');
	Route::post('/', 'HostController@store');
});